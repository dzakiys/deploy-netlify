/* eslint-disable tailwindcss/no-arbitrary-value */
import type { Client } from "@/types";

import useSwr from "@/hooks/useSwr";
// import logoAlsPizza from "@public/images/home/testimonials/logoAlsPizza.png";
// import logoArtop from "@public/images/home/testimonials/logoArtop.png";
// import logoAtlas from "@public/images/home/testimonials/logoAtlas.png";
// import logoGajahDuduk from "@public/images/home/testimonials/logoGajahDuduk.png";
// import logoHijaz from "@public/images/home/testimonials/logoHijaz.png";
// import logoKetjubung from "@public/images/home/testimonials/logoKetjubung.png";
// import logoMangga from "@public/images/home/testimonials/logoMangga.png";
// import logoNurulHayat from "@public/images/home/testimonials/logoNurulHayat.png";

import { gql } from "graphql-request";
import Image from "next/image";
import React, { useMemo } from "react";

// const clients = [
//     { logo: logoArtop.src, label: "Logo Artop" },
//     { logo: logoHijaz.src, label: "Logo Hijaz" },
//     { logo: logoGajahDuduk.src, label: "Logo Gajah Duduk" },
//     { logo: logoMangga.src, label: "Logo Mangga" },
//     { logo: logoKetjubung.src, label: "Logo Ketjubung" },
//     { logo: logoNurulHayat.src, label: "Logo Aqiqoh Nurul Hayat" },
//     { logo: logoAtlas.src, label: "Logo Atlas" },
//     { logo: logoAlsPizza.src, label: "Logo Al's Pizza" },
// ];

// const mockClients = {
//     items: [
//         {
//             id: "1",
//             name: "John",
//             imageSrc: {
//                 sm: logoAtlas.src,
//             },
//             basic: {
//                 isActive: true,
//             },
//         },
//         {
//             id: "2",
//             name: "John",
//             imageSrc: {
//                 sm: logoKetjubung.src,
//             },
//             basic: {
//                 isActive: false,
//             },
//         },
//     ],
// };

const Clients = () => {
    const gqlQuery = gql`
        query getClients($keyword: String) {
            clients(keyword: $keyword, limit: 99) {
                items {
                    id
                    name
                    imageSrc {
                        md
                    }
                    basic {
                        isActive
                    }
                }
            }
        }
    `;

    const { data } = useSwr(gqlQuery);

    const items = useMemo<Client[]>(() => {
        return data?.clients.items || [];
    }, [data]);

    return (
        <div className="flex flex-col items-center gap-[34px]">
            <h1 className="text-center font-outfit text-[3rem] font-semibold text-orange-500 md:text-[4rem]">
                Client<span className="text-neutral-950"> Kami</span>
            </h1>
            <div className="flex flex-wrap content-center items-center justify-center gap-8 md:gap-16 ">
                {items.map(
                    ({ id, name, imageSrc, basic }) =>
                        basic?.isActive && <Image key={id} src={imageSrc?.md || "https://via.placeholder.com/119x56"} alt={`${name} logo`} width={194} height={90} className="h-auto w-auto" />
                )}
            </div>
        </div>
    );
};

export default Clients;
