/* eslint-disable tailwindcss/no-arbitrary-value */
"use client";
import type { Testimonial } from "@/types";

import Clients from "@/components/sections/Clients";
import Avatar from "@/components/ui/Avatar";
import Button from "@/components/ui/Button";
import Skeleton from "@/components/ui/Skeleton";
import useSwr from "@/hooks/useSwr";
import { ArrowLeft, ArrowRight } from "@/lib/icons";
import imageTesti1 from "@public/images/home/testimonials/Testi_1.png";

// eslint-disable-next-line import/order
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/react-splide/css/core";
import { gql } from "graphql-request";
import React, { useMemo, useRef } from "react";

// const mockTestimonies = {
//     items: [
//         {
//             id: "1",
//             name: "John",
//             bio: "UMKM A",
//             message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
//             imageSrc: {
//                 sm: "https://picsum.photos/720?4.431459713575512",
//             },
//             basic: {
//                 isActive: true,
//             },
//         },
//         {
//             id: "2",
//             name: "Doe",
//             bio: "UMKM B",
//             message:
//                 "Lorem ipsum dolor elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
//             imageSrc: {
//                 sm: "https://picsum.photos/720?4.431459713575512",
//             },
//             basic: {
//                 isActive: true,
//             },
//         },
//         {
//             id: "3",
//             name: "John Doe",
//             bio: "UMKM C",
//             message:
//                 "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu",
//             imageSrc: {
//                 sm: "https://picsum.photos/720?4.431459713575512",
//             },
//             basic: {
//                 isActive: true,
//             },
//         },
//         {
//             id: "2",
//             name: "Doe",
//             bio: "UMKM B",
//             message:
//                 "Lorem ipsum dolor amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
//             imageSrc: {
//                 sm: "https://picsum.photos/720?4.431459713575512",
//             },
//             basic: {
//                 isActive: true,
//             },
//         },
//         {
//             id: "3",
//             name: "John Doe",
//             bio: "UMKM C",
//             message:
//                 "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu",
//             imageSrc: {
//                 sm: "https://picsum.photos/720?4.431459713575512",
//             },
//             basic: {
//                 isActive: true,
//             },
//         },
//     ],
// };

const TestimonialCard = ({ name, bio, message, imageSrc }: Testimonial) => (
    <SplideSlide>
        <div className="ts-regular-normal flex h-[288px] flex-col items-start gap-6 overflow-hidden rounded-2xl bg-white p-4">
            <Avatar src={imageSrc.sm || imageTesti1.src} alt={name ? `${name} photo` : "User profile picture"} />
            <div className="flex grow">
                <p className="line-clamp-5 grow break-all font-normal text-neutral-950">{message || "-"}</p>
            </div>
            <p className="font-semibold text-orange-600">
                {name || "-"}
                <span className="text-neutral-600"> - {bio || "-"}</span>
            </p>
        </div>
    </SplideSlide>
);

const Testimonials = () => {
    const gqlQuery = gql`
        query getTestimonies($limit: Int) {
            testimonies(limit: $limit) {
                items {
                    id
                    name
                    bio
                    message
                    imageSrc {
                        sm
                    }
                    basic {
                        isActive
                    }
                }
            }
        }
    `;

    const { data, isLoading } = useSwr(gqlQuery);

    const items = useMemo<Testimonial[]>(() => {
        return data?.testimonies.items || [];
    }, [data]);

    const testimonies = items.filter((testimony) => testimony?.basic?.isActive);

    const splideRef = useRef<Splide | null>(null);

    const prevSlide = () => {
        if (splideRef.current) {
            splideRef.current.go("-1");
        }
    };

    const nextSlide = () => {
        if (splideRef.current) {
            splideRef.current.go("+1");
        }
    };
    return (
        <section className="flex w-full items-center justify-center overflow-hidden bg-primary-50">
            <div className="flex w-full max-w-screen-2xl shrink-0 flex-col items-center gap-[61px] px-[18px] py-16  md:px-12">
                <Clients />
                <div className="flex w-full max-w-[651px] flex-col items-center justify-center gap-[18px]">
                    <h1 className="items-center text-center font-outfit text-[3rem] font-semibold text-neutral-950 md:text-[4rem]">
                        Apa Kata<span className="text-info-500"> Mereka</span>?
                    </h1>
                    <p className="text-center font-inter text-xl font-normal text-neutral-600 md:text-2xl">Ratusan pimpinan perusahaan merasa puas dan terbantu dengan layanan kamila printing</p>
                </div>

                <div className="flex h-[296px] w-[1344px] flex-row items-center justify-center gap-5">
                    <Button aria-label="previous slide" iconOnly className="hidden bg-white drop-shadow-md lg:flex" onClick={prevSlide}>
                        <span className="sr-only">Previous Slide</span>
                        <ArrowLeft size={24} color="#0EA5E9" variant="Outline" />
                    </Button>
                    <div className="flex w-[1227px] flex-row gap-4">
                        {isLoading ? (
                            Array.from({ length: 3 }).map((_, index) => <Skeleton key={index} className="flex h-[288px] w-1/3" />)
                        ) : (
                            <Splide
                                ref={splideRef}
                                options={{
                                    rewind: true,
                                    loop: true,
                                    perPage: 3,
                                    perMove: 1,
                                    autoplay: true,
                                    fixedHeight: "100%",
                                    gap: 20,
                                    pagination: false,
                                    arrows: false,
                                }}
                            >
                                {testimonies.map((testimonial, id) => (
                                    <TestimonialCard key={id} {...testimonial} />
                                ))}
                            </Splide>
                        )}
                    </div>
                    <Button aria-label="next slide" iconOnly className="hidden bg-white drop-shadow-md lg:flex" onClick={nextSlide}>
                        <span className="sr-only">Next Slide</span>
                        <ArrowRight size={24} color="#0EA5E9" variant="Outline" />
                    </Button>
                </div>
            </div>
        </section>
    );
};

export default Testimonials;
