export const ROUTES = {
    HOME: "/",
    ABOUT: "/about",
    PORTFOLIO: "/portfolio",
    SERVICES: "/services",
} as const;
