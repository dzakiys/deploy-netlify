/* eslint-disable tailwindcss/no-arbitrary-value */
"use client";

import type { Portfolio } from "@/types";

import Skeleton from "@/components/ui/Skeleton";
import useSwr from "@/hooks/useSwr";
import { ROUTES } from "@/lib/functions/constants";
import { slugify } from "@/lib/utils/slugify";

import { gql } from "graphql-request";
import { CloseCircle } from "iconsax-react";
import Image from "next/image";
import Link from "next/link";
import { notFound } from "next/navigation";
import React, { useMemo } from "react";

// const mockPortfolios = {
//     items: [
//         {
//             id: "1",
//             name: "Portfolio 1",
//             body: "Description for Portfolio 1",
//             thumbnailSrc: {
//                 original: "https://picsum.photos/1080/720?9.148850036119189",
//             },
//             basic: {
//                 isActive: true,
//             },
//             images: [
//                 {
//                     id: "img1",
//                     pathSrc: {
//                         original: "https://picsum.photos/1080/720?9.148850036119189",
//                     },
//                 },
//                 {
//                     id: "img2",
//                     pathSrc: {
//                         original: "https://picsum.photos/1080/720?9.148850036119189",
//                     },
//                 },
//             ],
//         },
//         {
//             id: "2",
//             name: "Portfolio 2",
//             body: "Description for Portfolio 2",
//             thumbnailSrc: {
//                 original: "https://picsum.photos/1080/720?9.148850036119189",
//             },
//             basic: {
//                 isActive: false,
//             },
//             images: [
//                 {
//                     id: "img3",
//                     pathSrc: {
//                         original: "https://picsum.photos/1080/720?9.148850036119189",
//                     },
//                 },
//                 {
//                     id: "img4",
//                     pathSrc: {
//                         original: "https://picsum.photos/1080/720?9.148850036119189",
//                     },
//                 },
//             ],
//         },
//     ],
// };

const PortfolioDetails = (params: { slug: string }) => {
    const { slug } = params;

    const gqlQuery = gql`
        query getPortfolios($keyword: String) {
            portfolios(keyword: $keyword, limit: 1000) {
                items {
                    id
                    name
                    body
                    thumbnailSrc {
                        original
                    }
                    basic {
                        isActive
                    }
                    images {
                        id
                        pathSrc {
                            original
                        }
                    }
                }
            }
        }
    `;

    const { data, isLoading } = useSwr(gqlQuery);

    const items = useMemo<Portfolio[]>(() => {
        return data?.portfolios?.items || [];
    }, [data]);

    const category = items.find((item) => slugify(item.name!) === slug);

    if (!category) {
        notFound();
    }

    return (
        <>
            <div className="flex items-start justify-between">
                {isLoading ? <Skeleton className="w-[50%] brightness-75" /> : <h1 className="font-outfit text-5xl/normal font-medium text-neutral-950">{category?.name || "-"}</h1>}
                <Link href={ROUTES.SERVICES}>
                    <CloseCircle size="34" color="#475569" />
                </Link>
            </div>
            {isLoading ? (
                <>
                    <Skeleton className="w-full" />
                    <Skeleton className="w-[80%]" />
                </>
            ) : (
                <p className="ts-lg-normal font-normal text-neutral-600">{category?.body || "-"}</p>
            )}
            <div className="flex flex-col gap-4 rounded-3xl md:gap-6">
                <div className="relative aspect-[890/948] w-full rounded-3xl">
                    {isLoading ? (
                        <Skeleton />
                    ) : (
                        <Image
                            alt={category?.name || "-"}
                            fill
                            className="rounded-2xl object-cover object-center"
                            sizes="(min-width: 940px) 858px, calc(95.16vw - 17px)"
                            priority
                            src={(category?.images && category.images[0]?.pathSrc?.original) || "https://via.placeholder.com/900x400"}
                        />
                    )}
                </div>

                <div className="relative aspect-[890/394] w-full rounded-xl">
                    {isLoading ? (
                        <Skeleton />
                    ) : (
                        <Image
                            alt={category?.name || "-"}
                            fill
                            className="rounded-2xl object-cover object-center"
                            sizes="(min-width: 940px) 858px, calc(95.16vw - 17px)"
                            src={(category?.images && category.images[1]?.pathSrc?.original) || "https://via.placeholder.com/900x400"}
                        />
                    )}
                </div>
                <div className="flex flex-row gap-4 rounded-3xl md:gap-6">
                    <div className="relative aspect-[435/554] w-1/2 rounded-3xl">
                        {isLoading ? (
                            <Skeleton />
                        ) : (
                            <Image
                                alt={category?.name || "-"}
                                fill
                                className="rounded-2xl object-cover object-center"
                                sizes="(min-width: 940px) 417px, calc(46.94vw - 15px)"
                                src={(category?.images && category.images[2]?.pathSrc?.original) || "https://via.placeholder.com/400x600"}
                            />
                        )}
                    </div>
                    <div className="relative aspect-[435/554] w-1/2 rounded-3xl">
                        {isLoading ? (
                            <Skeleton />
                        ) : (
                            <Image
                                alt={category?.name || "-"}
                                fill
                                className="rounded-2xl object-cover object-center"
                                sizes="(min-width: 940px) 417px, calc(46.94vw - 15px)"
                                src={(category?.images && category.images[3]?.pathSrc?.original) || "https://via.placeholder.com/400x600"}
                            />
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};

export default PortfolioDetails;
