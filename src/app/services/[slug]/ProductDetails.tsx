/* eslint-disable prettier/prettier */
/* eslint-disable tailwindcss/no-arbitrary-value */
"use client";

import type { StoreCategory, StoreProduct } from "@/types";

import Skeleton from "@/components/ui/Skeleton";
import useSwr from "@/hooks/useSwr";
import { ROUTES } from "@/lib/functions/constants";
import { slugify } from "@/lib/utils/slugify";

import { gql } from "graphql-request";
import { CloseCircle } from "iconsax-react";
import Image from "next/image";
import Link from "next/link";
import React, { useMemo } from "react";


type ItemType = StoreCategory & {
    products: StoreProduct[];
};

// const mockStoreCategories = {
//     items: [
//       {
//         id: '1',
//         name: 'Category 1',
//         slug: 'category-1',
//         description: 'Description for Category 1',
//         products: [
//           {
//             id: '101',
//             name: 'Product 1',
//             thumbnailSrc: {
//               original: 'https://via.placeholder.com/100',
//             },
//             images: [
//               {
//                 pathSrc: {
//                   original: 'https://via.placeholder.com/500',
//                 },
//               },
//             ],
//             category: {
//               name: 'Category 1',
//             },
//             basic: {
//               isActive: true,
//             },
//           },
//           
//         ],
//       },
//       
//     ],
//   };

const ProductDetails = (params: { slug: string }) => {
    const { slug } = params;

    const gqlQuery = gql`
    query getStoreCategories($keyword: String) {
        storeCategories(keyword: $keyword, limit: 1000) {
            items {
                id
                name
                slug
                description
                products {
                    id
                    name
                    thumbnailSrc {
                        original
                    }
                    images {
                        pathSrc {
                            original
                        }
                    }
                    category {
                        name
                    }
                    basic {
                        isActive
                    }
                }
            }
        }
    }`;
    
    const { data, isLoading } = useSwr(gqlQuery);

    const items = useMemo<ItemType[]>(() => {
        return data?.storeCategories?.items || [];
    }, [data]);

    const category = items.find((item) => (item.slug || slugify(item.name!)) === slug);

    // if (isError || !data || !category) {
    //     return notFound();
    // }

    return (
        <>
            <div className="flex items-start justify-between">
                {isLoading ? 
                 <Skeleton className="w-[50%] brightness-75" /> :
                <h1 className="font-outfit text-5xl/normal font-medium text-neutral-950">{category?.name || "-"}</h1>
                
            }
                <Link href={ROUTES.SERVICES}>
                    <CloseCircle size="34" color="#475569" />
                </Link>
            </div>
            {isLoading ? (
        <>
          <Skeleton className="w-full" />
          <Skeleton className="w-[80%]" />
        </>
      ) : (
        <p className="ts-lg-normal font-normal text-neutral-600">
          {category?.description || "-"}
        </p>
      )}
            <div className="flex flex-col gap-4 rounded-3xl md:gap-6">
                    <div className="relative aspect-[890/948] w-full rounded-3xl">
                        {isLoading ? (
                            <Skeleton />
                        ) : ( 
                            <Image
                                alt={category?.products[0]?.name || "-"}
                                fill
                                className="rounded-2xl object-cover object-center"
                                sizes="(min-width: 940px) 858px, calc(95.16vw - 17px)"
                                priority
                                src={category?.products[0]?.thumbnailSrc?.original || (category?.products[0]?.images && category?.products[0]?.images[0]?.pathSrc.original) || "https://via.placeholder.com/900x400"}
                            />
                        )}
                    </div>

                    <div className="relative aspect-[890/394] w-full rounded-xl">
                        {isLoading ? (
                            <Skeleton />
                        ) : (
                            <Image
                                alt={category?.products[1]?.name || "-"}
                                fill
                                className="rounded-2xl object-cover object-center"
                                sizes="(min-width: 940px) 858px, calc(95.16vw - 17px)"
                                src={category?.products[1]?.thumbnailSrc?.original || (category?.products[1]?.images && category?.products[1]?.images[0]?.pathSrc.original) || "https://via.placeholder.com/900x400"}
                            />
                        )}
                    </div>

                <div className="flex flex-row gap-4 rounded-3xl md:gap-6">
                        <div className="relative aspect-[435/554] w-1/2 rounded-3xl">
                            {isLoading ? (
                                <Skeleton />
                            ) : (
                                <Image
                                    alt={category?.products[2]?.name || "-"}
                                    fill
                                    className="rounded-2xl object-cover object-center"
                                    sizes="(min-width: 940px) 417px, calc(46.94vw - 15px)"
                                    src={
                                        category?.products[2]?.thumbnailSrc?.original || (category?.products[2]?.images && category?.products[2]?.images[0]?.pathSrc.original) || "https://via.placeholder.com/600x400"
                                    }
                                />
                            )}
                        </div>
                        <div className="relative aspect-[435/554] w-1/2 rounded-3xl">
                            {isLoading ? (
                                <Skeleton />
                            ) : (
                                <Image
                                    alt={category?.products[3]?.name || "-"}
                                    fill
                                    className="rounded-2xl object-cover object-center"
                                    sizes="(min-width: 940px) 417px, calc(46.94vw - 15px)"
                                    src={
                                        category?.products[3]?.thumbnailSrc?.original || (category?.products[3]?.images && category?.products[3]?.images[0]?.pathSrc.original) || "https://via.placeholder.com/400x600"
                                    }
                                />
                            )}
                        </div>
                </div>
            </div>
        </>
    );
};

export default ProductDetails;
